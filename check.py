import glob
import hackerboards_utils.check as hb

result = {
    'ok': [],
    'fail': [],
}
for file in glob.glob("dataset/*/*.yaml"):
    try:
        board = hb.check(file)
        if len(board['type']) > 0 or len(board['relations']) or len(board['missing']) > 0 or len(board['other']) > 0:
            result['fail'].append(file)
        else:
            result['ok'].append(file)
    except:
        print(f"Failed on {file}")
        raise

print()
print('-------------')
print(f"{len(result['ok'])} boards correct, {len(result['fail'])} boards have issues...")
for b in result['fail']:
    print(f"- {b}")
if len(result['fail']) > 0:
    exit(1)
