#!/usr/bin/env python3
import argparse
import glob
import os
import re

import yaml
import tabulate
import humanize

integers = [
    'cluster1_count',
    'cluster2_count',
    'cluster3_count',
    'gpu_count',
    'cpu_embedded_mcu_count',
    'npu_count',
    'cpu_board_mcu_count',
    'fpga_count',
    'memory_max',
    'memory_slots',
    'memory_speed',
    'storage_spiflash_size',
    'storage_emmc_count',
    'storage_emmc_size',
    'sd_count',
    'sd_slots',
    'sata_count',
    'sata_slots',
    'sata_m2_slots',
    'sata_revision',
    'usb_count',
    'usb_slots',
    'usb_hosts',
    'usb_device',
    'usb_otg',
    'usb_typec'
    'usb_superspeed',
    'ethernet_count',
    'ethernet_100',
    'ethernet_1000',
    'ethernet_2500',
    'ethernet_5000',
    'ethernet_10000',
    'ethernet_40000',
    'ethernet_100000',
    'ethernet_200000',
    'ethernet_400000',
    'ethernet_copper',
    'ethernet_sfp',
    'display_count',
    'video_composite_count',
    'video_composite_slots',
    'video_svideo_count',
    'video_svideo_slots',
    'video_hdmi_count',
    'video_hdmi_slots',
    'video_displayport_count',
    'video_displayport_slots',
    'video_vga_count',
    'video_vga_slots',
    'video_dpi_count',
    'video_dsi_count',
    'video_lvds_count',
    'camera_count',
    'camera_mipi',
    'camera_parallel',
    'audio_line_in',
    'audio_mic_in',
    'audio_mic_onboard',
    'audio_spdif_in',
    'audio_spdif_out',
    'audio_i2s',
    'wireless_wifi_2g_level',
    'wireless_wifi_5g_level',
    'wireless_wifi_chains',
    'gpio_count',
    'gpio_adc_count',
    'gpio_dac_count',
    'gpio_pwm_count',
    'i2c_count',
    'gpio_can_count',
    'gpio_spi_count',
    'uart_count',
    'width',
    'height',
    'depth',
]

floats = [
    'cluster1_speed',
    'cluster2_speed',
    'cluster3_speed',
    'gpu_speed',
    'power_input_voltage_min',
    'power_input_voltage_max',
    'power_input_current_min',
    'power_input_current_max',
    'temperature_min',
    'temperature_max',
]

booleans = [
    'memory_ecc',
    'memory_sodimm',
    'memory_ddr',
    'memory_ddr2',
    'memory_ddr3',
    'memory_ddr3l',
    'memory_ddr4',
    'memory_ddr4lp',
    'storage_spiflash_bootable',
    'storage_emmc_bootable',
    'sd_bootable',
    'sata_bootable',
    'usb_bootable',
    'ethernet_bootable',
    'ethernet_wol',
    'ethernet_poe_passive',
    'ethernet_poe_active',
    'wireless_wifi_onboard',
    'wireless_wifi_external_antenna',
    'wireless_bluetooth_onboard',
    'wireless_bluetooth_ble',
    'wireless_bluetooth_audio',
    'wireless_lora_onboard',
    'modem_onboard',
    'rtc',
    'ir_receiver',
    'ir_transmitter',
    'gpio_compatible_shield',
    'gpio_compatible_hat',
    'mounting_holes',
    'battery_connector',
    'deprecated',
]

binarysize = [
    'memory_max'
]

# TODO: arrays = [ 'software_operating_system ']

RE_OPERATOR = re.compile(r'(==|>=|<=|>|<|!=|~=)')


def parse_number(value):
    mult = 1
    if value[-1] == 'K':
        mult = 1024
    elif value[-1] == 'M':
        mult = 1024 * 1024
    elif value[-1] == 'G':
        mult = 1024 * 1024 * 1024
    if mult > 1:
        value = value[:-1]
    value = float(value) * mult
    return value


def validate_filters(select, is_set=False):
    for requirement in select:
        if is_set and '=' not in requirement:
            print(f"Invalid field update '{requirement}'. Format should be 'key=value' or 'key=\"value\"'")
            exit(1)

        if not is_set:
            operators = ['==', '>=', '<=', '>', '<', '!=', '~=']
            for op in operators:
                if op in requirement:
                    break
            else:
                print(f"Invalid filter '{requirement}'")
                print("Valid filter formats:")
                for op in operators:
                    print(f"    key{op}value")
                exit(1)


def load_dataset(path, select):
    result = []
    for path in glob.glob(os.path.join(path, '*/*.yaml')):
        with open(path, 'r') as handle:
            raw = yaml.load(handle, Loader=yaml.CLoader)

        for requirement in select:

            key, op, value = RE_OPERATOR.split(requirement, maxsplit=1)

            if key not in raw:
                raise ValueError(f"Unknown key {key}")

            if isinstance(raw[key], int):
                value = int(parse_number(value))
            elif isinstance(raw[key], float):
                value = parse_number(value)
            elif isinstance(raw[key], bool):
                value = bool(value)

            if op == '==':
                match = raw[key] == value
            elif op == '>=':
                match = raw[key] >= value
            elif op == '<=':
                match = raw[key] <= value
            elif op == '>':
                match = raw[key] > value
            elif op == '<':
                match = raw[key] > value
            elif op == '!=':
                match = raw[key] != value
            elif op == '~=':
                match = value in raw[key]
            else:
                raise ValueError("Invalid operator")

            if not match:
                break
        else:
            result.append((path, raw))
    return result


def main():
    parser = argparse.ArgumentParser(description="YAML bulk edit tool")
    parser.add_argument('--root', default=os.getcwd(), help='Specify the path to the export folder')
    parser.add_argument('--select', action='append', help='Add filter to select boards to edit')
    parser.add_argument('--set', action='append', help='Change value in the selected boards')
    args = parser.parse_args()

    if args.select:
        validate_filters(args.select)

    if args.set:
        validate_filters(args.set, is_set=True)

    boards = load_dataset(args.root, args.select or [])

    if args.set:
        rows = []
        headers = ['manufacturer', 'name']
        for change in args.set or []:
            key, value = change.split('=', maxsplit=1)
            headers.append(key)

        for board in boards:
            row = [board[1]['manufacturer'], board[1]['name']]
            for change in args.set or []:
                key, value = change.split('=', maxsplit=1)
                if key.endswith('//'):
                    key = key[:-2]
                    seperator = value[0]
                    temp = value.replace(r'\/', '\0')
                    parts = temp[1:].split(seperator)
                    search = parts[0].replace('\0', r'\/')
                    replace = parts[1].replace('\0', r'\/')
                    flags = ''
                    if len(parts) > 2:
                        flags = parts[2]
                    real_flags = 0
                    count = 1
                    if 'i' in flags:
                        real_flags |= re.IGNORECASE
                    if 'm' in flags:
                        real_flags |= re.MULTILINE
                    if 'x' in flags:
                        real_flags |= re.VERBOSE
                    if 's' in flags:
                        real_flags |= re.DOTALL
                    if 'g' in flags:
                        count = 0
                    if 'u' in flags:
                        real_flags |= re.UNICODE
                    if 'a' in flags:
                        real_flags |= re.ASCII

                    value = re.sub(search, replace, board[1][key], count, real_flags)
                else:
                    if key in integers:
                        value = int(parse_number(value))
                    elif key in floats:
                        value = parse_number(value)
                    elif key in booleans:
                        value = bool(value)
                    elif isinstance(board[1][key], int):
                        value = int(parse_number(value))
                    elif isinstance(board[1][key], float):
                        value = parse_number(value)
                    elif isinstance(board[1][key], bool):
                        value = bool(value)

                old = board[1][key]
                new = value
                if key in binarysize:
                    old = humanize.naturalsize(old, binary=True)
                    new = humanize.naturalsize(new, binary=True)
                change = f'{old} -> {new}'
                row.append(change)

                board[1][key] = value

            with open(board[0], 'w') as handle:
                yaml.dump(board[1], handle, default_flow_style=False)

            rows.append(row)
        print(f"Updated {len(rows)} boards:")
        print(tabulate.tabulate(rows, headers=headers))

    else:
        print("Results of search query:")
        keys = set()
        if args.select:
            for requirement in args.select:
                key, op, value = RE_OPERATOR.split(requirement, maxsplit=1)
                keys.add(key)

        headers = ['manufacturer', 'name']
        for key in keys:
            headers.append(key)

        rows = []
        for path, board in boards:
            row = [board['manufacturer'], board['name']]
            for key in keys:
                val = board[key]
                if key in binarysize:
                    val = humanize.naturalsize(val, binary=True)
                row.append(val)
            rows.append(row)
        print(tabulate.tabulate(rows, headers=headers))


if __name__ == '__main__':
    main()
